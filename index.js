const bcoin = require('bcoin');

const node = new bcoin.FullNode({
 
  logConsole: true,
  logLevel: 'debug',
  memory: false,
  workers: true,
  listen: true,
  prune: true,
  mempoolSize: 500,
  replaceByFee: true,
  maxOutbound: 6,
});

(async () => {
  await node.open();
  await node.ensure();
  await node.connect();

  node.on('connect', (entry, block) => {
    console.log('%s (%d) added to chain.', entry.rhash(), entry.height);
  });

  node.on('tx', (tx) => {
    console.log('%s added to mempool.', tx.txid());
  });

  node.startSync();
})().catch((err) => {
  console.error(err.stack);
  process.exit(1);
});
