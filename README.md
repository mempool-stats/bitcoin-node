# Bitcoin Node

Facilitates the running of a bitcoin node, collection, and ingestion of pending transaction data.

## Purpose
- Get the "fullest" view of the mempool as possible
- To create an API that can be used to get new pending transactions
    
### Requirements:
- Must have [make](http://man7.org/linux/man-pages/man1/make.1.html) installed.
- You need to have a `$HOME/.bcoin/blocks/index/` this path created

### Usage

Clone, install, then run.
```
git clone git@gitlab.com:mempool-stats/bitcoin-node.git 
npm install
npm start # run script with `forever`
```

Stops the running script.
```
npm stop
```

Notes: 
Currently `mempoolSize` is set to 500MB. It is possible that this is too low and
that transactions might get missed if the pool gets too full. Especially in this case,
since we are trying to collect as many pending transactions as possible.

